﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    public float jumpForce = 400f; //float for the jump force used to propel the player
    protected float moveSmoothing = 0.5f; //float for movement smoothing when the player runs
    public bool airControl; //bool for air control
    public LayerMask whatIsGround; //a layer mask to tell what is ground
    public Transform groundCheck; //transform to hold the ground check game object 
    public Transform groundDectection; //transform to hold the ground detection game object
    public float moveSpeed;  //float to hold the move speed
    protected const float groundRadius = 0.2f; //float for the grounded radius
    protected bool isGrounded; //bool to tell if the player is grounded or not
    protected Rigidbody2D rb; //reference to the game objects rigidbody
    protected SpriteRenderer sr; //reference to the game object sprite renderer
    public Vector3 velocity = Vector3.zero; //vector3 to hold the velocity
    public Animator anim; //reference to the animator
    protected bool movingRight = true; //bool to tell if an AI is moving right or not
    protected int health; //int to hold the health of the object
    public int starthealth; //value to modify the health of the object

    public virtual void Move(float move, bool jump)
    {

    }

    public virtual void Patrol()
    {

    }

    public virtual void TakeDamage(int damage)
    {

    }

    public virtual void Die()
    {

    }

    public virtual void FSM()
    {

    }
}
