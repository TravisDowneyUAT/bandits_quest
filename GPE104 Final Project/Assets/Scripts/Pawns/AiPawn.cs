﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiPawn : Pawn
{
    public GameObject coin; //reference to the coin game object
    public GameObject gem; //reference to the gem game object

    private void Start()
    {
        //setting the enemies health equal to the start health defined in the inspector
        health = starthealth;
    }

    public override void Patrol()
    {
        //move the enemy to the right by the move speed variable
        transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);

        //shoot a ray down to find ground
        RaycastHit2D ground = Physics2D.Raycast(groundDectection.position, Vector2.down, 2f);

        //if the ray doesn't hit a collider and the enemy is moving right then
        //rotate the enemy sprite -180 degrees on the y-axis and set movingRight to false
        //else if the enemy is moving left then reset their rotation and set movingRight to true
        if(ground.collider == false)
        {
            if(movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
    }

    public override void TakeDamage(int damage)
    {
        health -= damage; //decrease the health based on the damage taken

        anim.SetTrigger("Hurt"); //set the Hurt trigger in the animator

        //if the health is reduced to 0 then run the Die method
        if (health <= 0)
        {
            Die();
        }
    }

    public override void Die()
    {
        anim.SetBool("isDead", true); //set the isDead bool to true
        //Instantiate a coin and a gem at the dead enemies position
        Instantiate(coin, transform.position, Quaternion.identity);
        Instantiate(gem, transform.position, Quaternion.identity);
        gameObject.SetActive(false); //deactivate the game object
    }
}
