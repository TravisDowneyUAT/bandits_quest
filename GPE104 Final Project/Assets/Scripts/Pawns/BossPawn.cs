﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossPawn : Pawn
{
    public Transform attackPoint; //reference to the position of the attack point
    public float attackRange = 0.6f; //float to hold the attack range
    public LayerMask players; //layer mask to define the player
    public int attackDamage; //int to hold how much attack damage the boss does
    


    [HideInInspector]
    public enum States //enum list to hold the states of the boss
    {
        Idle,
        Move,
        Attack
    }
    public States curState; //variable to hold the current state the boss is in

    private bool inRange = false; //bool to detect if the player is in range
    private bool canAttack = false; //bool to tell if the boss can attack or not

    private GameObject player; //reference to the player

    // Start is called before the first frame update
    void Start()
    {
        health = starthealth; //set the bosses health equal to the start health variable
        player = GameObject.FindGameObjectWithTag("Player"); //set the reference to the player in the game
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void FSM()
    {
        //switch throught the states based on what the current state is
        switch (curState)
        {
            case States.Idle:
                break;
            case States.Move:
                if(inRange == true)
                {
                    Vector2.MoveTowards(transform.position, player.transform.position, moveSpeed * Time.deltaTime);
                }
                break;
            case States.Attack:
                if(canAttack == true)
                {
                    Attack();
                }
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //if the player enters the trigger then set the inRange bool in the animator equal to true
        //set the current state to the move state
        //and set the inRange bool to true
        if (col.gameObject.CompareTag("Player"))
        {
            anim.SetBool("inRange", true);
            curState = States.Move;
            inRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        //if the player exits the trigger set the inRange bool in the animator equal to false
        //set the current state to the idle state
        //and set the inRange bool to false
        if (col.gameObject.CompareTag("Player"))
        {
            anim.SetBool("inRange", false);
            curState = States.Idle;
            inRange = false;
        }
    }

    void Attack()
    {
        //set the canAttack trigger in the animator
        anim.SetTrigger("canAttack");
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, players);
        //for each enemy hit in the hitEnemies array run the take damage method
        foreach (Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<PlayerPawn>().TakeDamage(attackDamage);
        }
    }

    public override void TakeDamage(int damage)
    {
        //decrease the boss's health based on how much damage has been taken
        health -= damage;

        anim.SetTrigger("Hurt"); //set the Hurt trigger in the animator

        //if the health is reduced to zero then run the Die method
        if(health <= 0)
        {
            Die();
        }
    }

    public override void Die()
    {
        //set the isDead bool in the animator to true
        anim.SetBool("isDead", true);
        //load the Victory scene
        SceneManager.LoadScene("Victory");
        //deactivate the game object
        gameObject.SetActive(false);
    }
}
