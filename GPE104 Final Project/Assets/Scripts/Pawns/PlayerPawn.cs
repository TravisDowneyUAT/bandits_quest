﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerPawn : Pawn
{
    public UnityEvent OnLandEvent; //reference to the OnLandEvent
    private GameManager manager; //reference to the game manager
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>(); //reference the Rigidybody2D component on the player
        sr = GetComponent<SpriteRenderer>(); //reference the SpriteRenderer component on the player

        //if there is no OnLandEvent then make a new UnityEvent
        if (OnLandEvent == null)
        {
            OnLandEvent = new UnityEvent();
        }
        //set the reference to the manager to the game manager object in the scene
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void FixedUpdate()
    {
        //make a new bool to reference the isGrounded bool
        bool wasGrounded = isGrounded;
        isGrounded = false; //set isGrounded to false

        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundRadius, whatIsGround);
        /*as long as a collider is hit then set isGrounded to true and if wasGrounded is the opposite
         then invoke the on land event*/
        for (int i = 0; i < colliders.Length; i++)
        {
            isGrounded = true;
            if (!wasGrounded)
            {
                OnLandEvent.Invoke();
            }
        }
    }

    public override void Move(float move, bool jump)
    {
        //make a new vector 3 and set it equal to a vector 2 with the move parameter multipled by 10 for the x
        //and the linear velocity of the rigidbody on the y-axis
        Vector3 targetVel = new Vector2(move * 10f, rb.velocity.y);
        //set the veloctiy of the rigidbody to a SmoothDamp on a Vector3 based on the veloctiy
        //of the rigidbody, the target velocity vector, the velocity defined in the Pawn sript
        //and the moveSmoothing variable
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVel, ref velocity, moveSmoothing);

        //if the move parameter is less than 0 then don't flip the sprite on the x-axis
        if (move < 0)
        {
            sr.flipX = false;
        }
        //if the move parameter is greater than 0 then flip the sprite on the x-axis
        else if (move > 0)
        {
            sr.flipX = true;
        }

        //if the player is grounded and the jump parameter is true then set isGrounded to false
        //and propel the player up on the y-axis based on the jumpForce variable
        if (isGrounded && jump)
        {
            isGrounded = false;
            rb.AddForce(new Vector2(0f, jumpForce));
        }
    }

    public override void TakeDamage(int damage)
    {
        //decrease the health based on the amount of damage taken
        health -= damage;

        //if the health is decreased to 0 then destroy the player's game object and decrease the player's lives
        if(health <= 0)
        {
            Destroy(gameObject);
            manager.DecreaseLives();
        }
    }
}
