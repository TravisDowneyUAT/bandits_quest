﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiController : Controller
{
    public AiPawn molePawn; //reference to the AiPawn script on the mole object
    public AiPawn treantPawn; //reference to the AiPawn script on the treant object

    // Update is called once per frame
    void Update()
    {
        //run the patrol method on both the mole and treant
        molePawn.Patrol();
        treantPawn.Patrol();
    }
}
