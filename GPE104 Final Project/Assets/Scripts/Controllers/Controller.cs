﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    [HideInInspector]
    public float horizontalMove = 0f; //variable for movement along the horizontal axis
    public float runSpeed; //variable to hold how fast we want the player to run
    [HideInInspector]
    public bool jump; //bool for if we want the player to be jumping or not
    public Animator anim; //reference to an animator component 
    public Transform attackPoint; //reference to the position of an attack point
    public float attackRange = 0.5f; //variable to define the attack range
    public LayerMask enemies; //layer mask to hold the enemies
    public int attackDamage; //variable to hold the amount of attack damage the character does

    public virtual void GetPlayerInput()
    {
        
    }
    public virtual void OnLanding()
    {

    }

    public virtual void Attack()
    {

    }

    public virtual void TakeDamage(int damage)
    {

    }
}
