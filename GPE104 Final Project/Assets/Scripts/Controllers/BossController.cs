﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : Controller
{
    public BossPawn pawn; //reference to the BossPawn

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pawn.FSM(); //running the FSM method in the BossPawn script
    }
}
