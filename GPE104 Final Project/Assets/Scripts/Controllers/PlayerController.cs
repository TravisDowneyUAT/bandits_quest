﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : Controller
{
    public PlayerPawn pawn; //reference to the PlayerPawn

    private void Start()
    {
        
    }

    private void Update()
    {
        //set the horizontalMove variable from the Controller script to the input from the Horizontal axis multipled by the runSpeed variable
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        //set the Speed float in the animator to the abosolute value of the horizontalMove variable
        anim.SetFloat("Speed", Mathf.Abs(horizontalMove));
        //if the player presses the Jump button then set the jump bool equal to true
        //and set the isJumping bool in the animator equal to true
        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            anim.SetBool("isJumping", true);
        }
        //if the players presses the Fire1 button then run the Attack method
        if (Input.GetButtonDown("Fire1"))
        {
            Attack();
        }
    }

    private void FixedUpdate()
    {
        GetPlayerInput();

        //if the player presses the keys on the horizontal axis then play the PlayerWalk clip
        if (Input.GetButtonDown("Horizontal"))
        {
            FindObjectOfType<AudioManager>().Play("PlayerWalk");
        }
    }

    public override void Attack()
    {
        //set the Attack trigger in the animator
        anim.SetTrigger("Attack");
        //make a new collider2d array and populate with any collider the player hits on the enemies LayerMask
        Collider2D[] hitEnemies =  Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemies);
        //for each collider2d hit in the hitEnemies array run the TakeDamage method on whatever enemy was hit
        foreach(Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<AiPawn>().TakeDamage(attackDamage);
            enemy.GetComponent<BossPawn>().TakeDamage(attackDamage);
        }

    }

    public override void OnLanding()
    {
        //set the isJumping bool to false when the player lands on ground
        anim.SetBool("isJumping", false);
    }

    public override void GetPlayerInput()
    {
        //move the player based on the input from horizontalMove multipled by a fixed amount of time per frame draw
        pawn.Move(horizontalMove * Time.fixedDeltaTime, jump);
        jump = false;
    }
}
