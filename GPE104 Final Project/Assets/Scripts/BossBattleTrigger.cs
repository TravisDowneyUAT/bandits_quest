﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBattleTrigger : MonoBehaviour
{
    public GameObject wall; //reference to a game object

    private void OnTriggerEnter2D(Collider2D col)
    {
        //if the player enters the trigger then activate the game object
        if (col.gameObject.CompareTag("Player"))
        {
            wall.SetActive(true);
        }
    }
}
