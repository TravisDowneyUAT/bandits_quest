﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance; //static reference to the game manager

    public GameObject player; //reference to the player

    [HideInInspector]
    public Vector2 lastCheckpointPos; //reference to a Vector 2 for a checkpoint position

    private int playerLives; //private int to hold player lives
    private int treasureFound; //private int to hold the amount of treasure found

    public Text playerLivesText; //reference to the player lives text in the canvas
    public Text treasureFoundText; //reference to the treasure found text in the canvas

    // Start is called before the first frame update
    void Awake()
    {
        //if there is no instance of the game manager in the scene, make this game object the new instance
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        GameStart();
    }

    void GameStart()
    {
        playerLives = 3; //set the player's lives equal to 3
        treasureFound = 0; //set the treasure found equal to 0

        playerLivesText.text = "Lives: " + playerLives; //update the UI to show the proper amount of player lives
        treasureFoundText.text = "Treasure: " + treasureFound; //update the UI to show the proper amount of treasure found
    }

    //increase the treasure found based on however much of an amount is defined by the parameter
    public void IncreaseTreasure(int amt)
    {
        treasureFound += amt;

        treasureFoundText.text = "Treasure: " + treasureFound; //update the UI with the new amount
    }

    //decrease the player's lives
    //update the UI with the new amount
    //if the player's lives decrease to 0 then load the GameOver scene
    public void DecreaseLives()
    {
        playerLives--;

        playerLivesText.text = "Lives: " + playerLives;

        if (playerLives <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
