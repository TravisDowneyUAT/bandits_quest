﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAttack : MonoBehaviour
{
    public GameObject arrow; //reference to a game object

    private void OnTriggerEnter2D(Collider2D col)
    {
        //if the player enters the trigger then spawn in an arrow with a -90 rotation on the z-axis
        if (col.gameObject.CompareTag("Player"))
        {
            Instantiate(arrow, transform.right, Quaternion.Euler(0f, 0f, -90f));
        }
    }
}
