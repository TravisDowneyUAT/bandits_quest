﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private GameManager manager; //reference to the game manager
    public Animator anim; //reference to an animator

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    /*If the player passes through the checkpoint set the last checkpoint position variable
     in the game manager to the checkpoints posiiton*/
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            manager.lastCheckpointPos = transform.position;
            anim.SetBool("isActive", true);
        }
    }
}
