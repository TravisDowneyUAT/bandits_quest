﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    GameObject target; //reference to a target
    Vector3 offset; //reference to set an offset

    // Start is called before the first frame update
    void Start()
    {
        //setting the target equal to the player
        target = GameObject.FindGameObjectWithTag("Player");
        //setting the offset to the position of the camera minus the position of the player
        offset = transform.position - target.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //update the camera's position based on the player's position plus the offset
        transform.position = target.transform.position + offset;
    }
}
