﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemPickup : MonoBehaviour
{
    private GameManager manager; //reference to the game manager
    private int amt = 100; //amount the gem is worth

    // Start is called before the first frame update
    void Start()
    {
        //setting the reference to the game manager object in the scene
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //if the player enters the trigger then run the IncreaseTreasure method in the game manager
        //and deactivate the game object
        if (col.gameObject.CompareTag("Player"))
        {
            manager.IncreaseTreasure(amt);
            gameObject.SetActive(false);
        }
    }
}
