﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float length; //float to hold the length of the sprite
    private float startPos; //float to hold the start position of the sprite
    public GameObject cam; //reference to a camera
    public float parallaxEffect; //public float to hold the amount of parallax effect for the given sprite

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position.x; //setting the start position to the x variable on the objects transform
        length = GetComponent<SpriteRenderer>().bounds.size.x; //setting the length to the bounds of the x axis on the sprite renderer
    }

    // Update is called once per frame
    void Update()
    {
        //making a new float to hold the position of the camera on the x-axis multiplied by 1 minus the parallax effect
        float temp = cam.transform.position.x * (1 - parallaxEffect);
        //making a new float to hold the position of the camera on the x-axis multiplied by the parallax effect
        float distance = cam.transform.position.x * parallaxEffect;

        //setting the position of the parallax object to a new Vector3 with the start position plus the distance on the x
        //and then the game objects position on the y and z-axes
        transform.position = new Vector3(startPos + distance, transform.position.y, transform.position.z);

        //if the temp variable is greater than the start position plus the length
        //then increase the start position by the length
        if(temp > startPos + length)
        {
            startPos += length;
        }
        //else if the the temp is less than the start position minus the length
        //then decrease the start position by the length
        else if(temp < startPos - length)
        {
            startPos -= length;
        }
    }
}
